# README #
# Analisi dei Requisiti : #

# Requisiti Funzionali dell'utente #
- L'utente potrà porre delle domande e rispondere ad altre domande già esistenti nel database

- L'utente potrà visualizzare in ordine cronologico le domande e le risposte se il database non è vuoto.

- L'utente dovrà effettuare il log-in previa registrazione se vorrà partecipare attivamente alle discussioni.

# Requisiti non funzionali #

- Il sistema permette la visualizzazione delle pagine a tutti gli utenti che visitano la pagina.
   
- Per poter usufruire del servizio di domanda e risposta, il sistema richiede la registrazione con username, password e email validi e in aggiunta campi opzionali come Nome e Cognome, Sesso,Data di nascita, Luogo di Nascita, Luogo di residenza o domicilio.

- Il sistema offre un database per gestire i dati persistenti.

- La pagina principale visualizza in ordine cronologico le domande, dalla più recente.
          
- Il sistema , per ogni domanda deve visualizzare l’ora, la data e l’user dell’utente che ha generato la domanda o risposta.


- Il sistema richiede che la domanda e la risposta comprendano solo testo rispettivamente max 300 caratteri e  max 500 caratteri.

- Il sistema non permette la cancellazione di domande e risposte agli utenti, possibile questo solo dall''utente admin.



-# Scopo del progetto #

-Lo scopo e' la progettazione di un sistema che gestisca domande e risposte di utenti registrati
- 
-# Fasi del lavoro #
-
-- Stesura del Glossario
-- Studio del dominio relativo al problema e la stesura di uno scenario di casi d'uso
-- Realizzazione dei diagramma delle classi
-- 
-- Realizzazione dei diagrammi OMT (fase di progettazione):
-modello ad oggetti
-modello dinamico
-modello funzionale
-comunicazione e traccia eventi
- Versioni
-
-Elenco delle versioni dei documenti
- Riunioni
-
-Elenco delle riunioni del gruppo 
-
-Per maggiori informazioni sulle attivita' da svolgere, consultare Corso di Ingegneria del software Laboratorio di Progettazione
-This README would normally document whatever steps are necessary to get your application up and running.
-
-### What is this repository for? ###
-
-* Quick summary
-* Version
-* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
-
-### How do I get set up? ###
-
-* Summary of set up
-* Configuration
-* Dependencies
-* Database configuration
-* How to run tests
-* Deployment instructions
-
-### Contribution guidelines ###
-
-* Writing tests
-* Code review
-* Other guidelines
Add a comment to this line
-
-### Who do I talk to? ###
-
-* Repo owner or admin
-* Other community or team contact