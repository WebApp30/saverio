package de.marco;

import de.marco.client.AskAnsTest;
import com.google.gwt.junit.tools.GWTTestSuite;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AskAnsSuite extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Tests for AskAns");
    suite.addTestSuite(AskAnsTest.class);
    return suite;
  }
}
