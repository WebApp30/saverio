package de.marco.shared;

import java.io.Serializable;

public class UnauthorizedException extends Exception implements Serializable {

	private static final long serialVersionUID = -1289089457487358614L;  

	public UnauthorizedException() {
	}

	@Override
	public String getMessage() {
		return "Solo gli utenti registrati possono eseguire questa azione";
	}
	
}
