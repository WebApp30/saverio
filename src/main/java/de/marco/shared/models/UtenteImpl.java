package de.marco.shared.models;

import java.io.Serializable;
import java.util.List;

public class UtenteImpl implements Serializable, Utente {
	
	private static final long serialVersionUID = -1614381101942583576L;
	
	/* OBBLIGATORI */
	private String username;
	private String password;
	private String email;
	/* FACOLTATIVI */
	private String nome;
	private String cognome;
	private String sesso;
	private String dataDiNascita;
	private String luogoDiNascita;
	private String indirizzo;

	private List<Long> idDomande;
	
	public UtenteImpl() {}
	
	public UtenteImpl(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}
	
	
	
	@Override
	public String getUsername() {
		return username;
	}
	
	@Override
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	@Override
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String getEmail() {
		return email;
	}
	
	@Override
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String getNome() {
		return nome;
	}
	
	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String getCognome() {
		return cognome;
	}
	
	@Override
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	@Override
	public String getSesso() {
		return sesso;
	}
	
	@Override
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	
	@Override
	public String getDataDiNascita() {
		return dataDiNascita;
	}
	
	@Override
	public void setDataDiNascita(String dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}
	
	@Override
	public String getLuogoDiNascita() {
		return luogoDiNascita;
	}
	
	@Override
	public void setLuogoDiNascita(String luogoDiNascita) {
		this.luogoDiNascita = luogoDiNascita;
	}
	
	@Override
	public String getIndirizzo() {
		return indirizzo;
	}
	
	@Override
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	@Override
	public List<Long> getIdDomande() {
		return idDomande;
	}
	
	@Override
	public void setIdDomande(List<Long> idDomande) {
		this.idDomande = idDomande;
	}
	
}
