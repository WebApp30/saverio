package de.marco.shared.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public interface Domanda extends Serializable {

	String getId();
	
	void setId(String id);
	
	String getTesto();

	void setTesto(String testo);
	
	String getAutore();

	void setAutore(String autore);

	Date getTime();

	void setTime(Date time);
	
	String getDisplayTime();

	List<Risposta> getRisposte();

	void setRisposte(List<Risposta> risposte);

}