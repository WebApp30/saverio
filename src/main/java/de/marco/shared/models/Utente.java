package de.marco.shared.models;

import java.io.Serializable;
import java.util.List;

public interface Utente extends Serializable{

	String getUsername();

	void setUsername(String username);

	String getPassword();

	void setPassword(String password);

	String getEmail();

	void setEmail(String email);

	String getNome();

	void setNome(String nome);

	String getCognome();

	void setCognome(String cognome);

	String getSesso();

	void setSesso(String sesso);

	String getDataDiNascita();

	void setDataDiNascita(String dataDiNascita);

	String getLuogoDiNascita();

	void setLuogoDiNascita(String luogoDiNascita);

	String getIndirizzo();

	void setIndirizzo(String indirizzo);

	List<Long> getIdDomande();

	void setIdDomande(List<Long> idDomande);

}