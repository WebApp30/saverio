package de.marco.shared.models;

import java.io.Serializable;
import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;

public class RispostaImpl implements Serializable, Risposta {

	private static final long serialVersionUID = -392537115354119026L;

	private String id;
	private String testo;
	private Utente autore;
	private Domanda domanda;
	private Date time;

	public RispostaImpl() {
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getTesto() {
		return testo;
	}

	@Override
	public void setTesto(String testo) {
		this.testo = testo;
	}

	@Override
	public Utente getAutore() {
		return autore;
	}

	@Override
	public void setAutore(Utente autore) {
		this.autore = autore;
	}

	@Override
	public Domanda getDomanda() {
		return domanda;
	}

	@Override
	public void setDomanda(Domanda domanda) {
		this.domanda = domanda;
	}

	@Override
	public Date getTime() {
		return time;
	}

	@Override
	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public String getDisplayTime() {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_MEDIUM).format(this.time);
	}

}
