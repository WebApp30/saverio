package de.marco.shared.models;

import java.io.Serializable;
import java.util.Date;

public interface Risposta extends Serializable {

	String getId();

	void setId(String id);

	String getTesto();

	void setTesto(String testo);

	Utente getAutore();

	void setAutore(Utente autore);

	Domanda getDomanda();

	void setDomanda(Domanda domanda);

	Date getTime();

	void setTime(Date time);

	String getDisplayTime();

}