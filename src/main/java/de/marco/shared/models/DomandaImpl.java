package de.marco.shared.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;

public class DomandaImpl implements Serializable, Domanda {
	
	private static final long serialVersionUID = 6958038833916553926L;
	
	private String id;
	private String testo;
	private String autore;
	private Date time;
	
	private List<Risposta> risposte;

	public DomandaImpl() {}
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getTesto() {
		return testo;
	}

	@Override
	public void setTesto(String testo) {
		this.testo = testo;
	}

	@Override
	public String getAutore() {
		return autore;
	}

	@Override
	public void setAutore(String autore) {
		this.autore = autore;
	}

	@Override
	public Date getTime() {
		return time;
	}

	@Override
	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public List<Risposta> getRisposte() {
		return risposte;
	}

	@Override
	public void setRisposte(List<Risposta> risposte) {
		this.risposte = risposte;
	}

	@Override
	public String getDisplayTime() {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_MEDIUM).format(this.time);
	}

}
