package de.marco.client;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;

import de.marco.client.ui.MainPage;
import de.marco.client.ws.DomandaService;
import de.marco.client.ws.DomandaServiceAsync;
import de.marco.client.ws.LoginService;
import de.marco.client.ws.LoginServiceAsync;
import de.marco.client.ws.UserService;
import de.marco.client.ws.UserServiceAsync;

/**
 * 
 * Singleton informazioni condivise app
 *
 */
public enum AppController {

	INSTANCE;

	public final DomandaServiceAsync domandaService = GWT.create(DomandaService.class);
	public final UserServiceAsync userService = GWT.create(UserService.class);
	public final LoginServiceAsync loginService = GWT.create(LoginService.class);
	private HTMLPanel mainPanel;
	private MainPage mainPage;
	private String loggedInUser = null;
	
	private AppController() {
		// When the app starts up, we need to know if someone is already logged in from last time
		loginService.getLoggedInUserName(new AsyncCallback<String>() {
			
			@Override
			public void onSuccess(String result) {
				loggedInUser = result;
			}

			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}
	

	public void setMainPanel(HTMLPanel panel) {
		this.mainPanel = panel;
	}

	public HTMLPanel getMainPanel() {
		return this.mainPanel;
	}

	public MainPage getMainPage() {
		return mainPage;
	}

	public void setMainPage(MainPage mainPage) {
		this.mainPage = mainPage;
	}

	public String getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(String loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	
}
