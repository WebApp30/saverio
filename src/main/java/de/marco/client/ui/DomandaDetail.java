package de.marco.client.ui;


import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperButton;
import com.vaadin.polymer.paper.widget.PaperToast;

import de.marco.client.AppController;
import de.marco.shared.models.Domanda;
import de.marco.shared.models.Risposta;

public class DomandaDetail extends Composite {

	private static DomandaDetailUiBinder uiBinder = GWT.create(DomandaDetailUiBinder.class);
	interface DomandaDetailUiBinder extends UiBinder<Widget, DomandaDetail> {
	}
	
	private Domanda myDomanda = null;

	@UiField DivElement domandaField;
	@UiField DivElement autore;
	@UiField DivElement data;
	@UiField DivElement testo;
	@UiField PaperButton answerButton;
	
	@UiField HTMLPanel risposteField;
	@UiField DivElement noRisposte;
	
	@UiField CreateRispostaForm createRisposta;
	@UiField PaperToast notLoggedIn;
	
	public DomandaDetail(String domandaId) {
		initWidget(uiBinder.createAndBindUi(this));
		init(domandaId);
	}
	
	@UiHandler("backButton")
	void goBack(ClickEvent ev) {
		HTMLPanel mainPanel = AppController.INSTANCE.getMainPanel();
		mainPanel.clear();
		mainPanel.add(new DomandaListView());
	}
	
	@UiHandler("answerButton")
	void answer(ClickEvent ev) {
		// Prevent unregistered users from posting an answer
		if (AppController.INSTANCE.getLoggedInUser() == null) {
			notLoggedIn.open();
			return;
		}
		createRisposta.setDomandaId(myDomanda.getId());
		createRisposta.open();
	}
	
	
	private void init(String id) {
		AppController.INSTANCE.domandaService.getDomandaById(id, new AsyncCallback<Domanda>() {
			
			@Override
			public void onSuccess(Domanda result) {
				myDomanda = result;

				autore.setInnerText(result.getAutore());
				data.setInnerText(result.getDisplayTime());
				testo.setInnerText(result.getTesto());
				List<Risposta> risposte = result.getRisposte();

				// ordina risposte in cronologicamente
				risposte.sort((r1, r2) -> r2.getTime().compareTo(r1.getTime()));
				
				if (risposte != null && risposte.size() > 0) {
						noRisposte.removeFromParent(); //remove m

						for (Risposta r : risposte) {
							risposteField.add(new RispostaListElement(r));
						}

				} else {
					risposteField.removeFromParent();
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				
			}
		});
	}

}
