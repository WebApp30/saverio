package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperIconButton;
import com.vaadin.polymer.paper.widget.PaperToast;
import de.marco.client.AppController;
import de.marco.shared.UnauthorizedException;
import de.marco.shared.models.Domanda;


public class DomandaListElement extends Composite {

	private static DomandaListElementUiBinder uiBinder = GWT.create(DomandaListElementUiBinder.class);

	interface DomandaListElementUiBinder extends UiBinder<Widget, DomandaListElement> {
	}

	private Domanda domanda = null;

	@UiField DivElement autore;
	@UiField DivElement data;
	@UiField DivElement testo;
	@UiField PaperIconButton deleteDomanda;

	public DomandaListElement(Domanda d) {
		initWidget(uiBinder.createAndBindUi(this));
		
		// Only admin can have a delete button
		String user = AppController.INSTANCE.getLoggedInUser();
		if (user == null || !user.equals("admin")) {
			deleteDomanda.removeFromParent();
		}

		domanda = d;
		autore.setInnerText(d.getAutore());
		data.setInnerText(d.getDisplayTime());
		testo.setInnerText(d.getTesto());
	}

	@UiHandler("detail")
	void showDetail(ClickEvent e) {
		HTMLPanel mainPanel = AppController.INSTANCE.getMainPanel();
		mainPanel.clear();
		mainPanel.add(new DomandaDetail(domanda.getId()));
	}

	@UiHandler("deleteDomanda")
	void delete(ClickEvent e) {
		try {
			AppController.INSTANCE.domandaService.removeDomanda(domanda.getId(), new AsyncCallback<Void>() {

				private HTMLPanel mainPanel = AppController.INSTANCE.getMainPanel();

				@Override
				public void onSuccess(Void result) {
					PaperToast toast = new PaperToast();
					toast.setText("Domanda eliminata.");

					// Refresh list view and add toast notification
					mainPanel.clear();
					mainPanel.add(new DomandaListView());
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); //  lambda
				}

				@Override
				public void onFailure(Throwable caught) {

					PaperToast toast = new PaperToast();
					toast.setText("Operazione fallita. Sei admin?");
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); //  lambda
				}
			});
		} catch (UnauthorizedException e1) {
			// l'utente non è registrato
			PaperToast toast = new PaperToast();
			toast.setText("Solo l'amministratore può eliminare una domanda.");
			AppController.INSTANCE.getMainPanel().add(toast);

			toast.ready((arg) -> {
				toast.open();
				return null;
			}); //  lambda
		}
	}
}
