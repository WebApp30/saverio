package de.marco.client.ui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperDialog;

import de.marco.client.AppController;
import de.marco.shared.models.Utente;

public class UsersList extends PaperDialog {

	private static UsersListUiBinder uiBinder = GWT.create(UsersListUiBinder.class);

	interface UsersListUiBinder extends UiBinder<Widget, UsersList> {
	}

	@UiField
	HTMLPanel lista;

	// @Override
	// public void ready(Function<?, ?> f) {
	//
	// AppController.INSTANCE.userService.getUtenti(new
	// AsyncCallback<List<Utente>>() {
	//
	// @Override
	// public void onSuccess(List<Utente> result) {
	// for ( Utente u : result )
	// lista.add(new UtentiListElement(u));
	//
	// }
	//
	// @Override
	// public void onFailure(Throwable caught) {
	// // TODO Auto-generated method stub
	//
	// }
	// });
	//
	// super.ready(f);
	// }
	//

	public UsersList(String st) {
		super(st);
		add(uiBinder.createAndBindUi(this));
		this.setEntryAnimation("fade-in-animation");
		this.setModal(true);
		this.addStyleName("userList-dialog");
		this.setHeight("70%");
		this.setWidth("30%");

		AppController.INSTANCE.userService.getUtenti(new AsyncCallback<List<Utente>>() {

			@Override
			public void onSuccess(List<Utente> result) {
				for (Utente u : result)
					lista.add(new UtentiListElement(u));

			}

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}
		});
	}
 
	@UiHandler("dismiss")
	void closeDialog(ClickEvent ev) {

		this.close();
	}

}
