package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperIconButton;
import com.vaadin.polymer.paper.widget.PaperToast;

import de.marco.client.AppController;
import de.marco.shared.UnauthorizedException;
import de.marco.shared.models.Risposta;

public class RispostaListElement extends Composite {

	private static RispostaListElementUiBinder uiBinder = GWT.create(RispostaListElementUiBinder.class);

	interface RispostaListElementUiBinder extends UiBinder<Widget, RispostaListElement> {
	}
	
	@UiField DivElement autore;
	@UiField DivElement data;
	@UiField DivElement testo;
	@UiField PaperIconButton deleteRisposta;
	
	private Risposta r;
	
	
	public RispostaListElement(Risposta r) {
		initWidget(uiBinder.createAndBindUi(this));

		// delete button
		String user = AppController.INSTANCE.getLoggedInUser();
		if (user == null || !user.equals("admin")) {
			deleteRisposta.removeFromParent();
		}
		this.r = r;
		autore.setInnerText(r.getAutore().getUsername());
		data.setInnerText(r.getDisplayTime());
		testo.setInnerText(r.getTesto());
	}
	
	@UiHandler("deleteRisposta")
	void delete(ClickEvent e) {
		try {
			AppController.INSTANCE.domandaService.removeRisposta(r.getDomanda().getId(), r, new AsyncCallback<Void>() {

				private HTMLPanel mainPanel = AppController.INSTANCE.getMainPanel();

				@Override
				public void onSuccess(Void result) {
					PaperToast toast = new PaperToast();
					toast.setText("Risposta eliminata.");

					// Refresh list view and add toast notification
					mainPanel.clear();
					mainPanel.add(new DomandaDetail(r.getDomanda().getId()));
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); // Very ugly lambda
				}

				@Override
				public void onFailure(Throwable caught) {

					PaperToast toast = new PaperToast();
					toast.setText("Operazione fallita. Sei admin?");
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); // Very ugly lambda
				}
			});
		} catch (UnauthorizedException e1) {
			// l'utente non è registrato
			PaperToast toast = new PaperToast();
			toast.setText("Solo l'amministratore può eliminare una risposta.");
			AppController.INSTANCE.getMainPanel().add(toast);

			toast.ready((arg) -> {
				toast.open();
				return null;
			}); // Very ugly lambda
		}
	}
	
}
