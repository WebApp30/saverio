package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperDialog;
import com.vaadin.polymer.paper.widget.PaperInput;
import com.vaadin.polymer.paper.widget.PaperToast;

import de.marco.client.AppController;

public class LoginForm extends PaperDialog {

	private static LoginFormUiBinder uiBinder = GWT.create(LoginFormUiBinder.class);

	interface LoginFormUiBinder extends UiBinder<Widget, LoginForm> {
	}

	@UiField
	PaperInput username;
	@UiField
	PaperInput password;

	public LoginForm(String st) {
		super(st);
		add(uiBinder.createAndBindUi(this));
		this.setEntryAnimation("fade-in-animation");
		this.addStyleName("login-dialog");
		this.setModal(true);
		this.setHeight("50%");
	}

	@UiHandler("loginBtn")
	void login(ClickEvent ev) {
		String usernameStr = username.getValue();
		String passwdStr = password.getValue();
		AppController.INSTANCE.loginService.login(usernameStr, passwdStr, new AsyncCallback<Boolean>() {

			private HTMLPanel mainPanel =  AppController.INSTANCE.getMainPanel();
			
			@Override
			public void onSuccess(Boolean successful) {
				closeDialog(null);
				PaperToast toast = new PaperToast();
				
				if (successful) {
					AppController.INSTANCE.setLoggedInUser(usernameStr);
					toast.setText("Login eseguito con successo."); 
					
				} else {
					toast.setText("Login fallito. Controlla le credenziali.");
					open();  // apre di nuovo il Dialog del login
				}
				mainPanel.clear();
				mainPanel.add(new DomandaListView());
				mainPanel.add(toast);

				toast.ready((arg) -> {
					toast.open();
					return null;
				}); // lambda
				
				AppController.INSTANCE.getMainPage().refreshMenu();				
			}

			@Override
			public void onFailure(Throwable caught) {
				closeDialog(null);
				PaperToast toast = new PaperToast("Si è verificato un errore durante il login.");
				AppController.INSTANCE.getMainPanel().add(toast);
				toast.open();
			}
		});
	}

	@UiHandler("dismiss")
	void closeDialog(ClickEvent ev) {
		username.setValue("");
		password.setValue("");
		this.close();
	}

}
