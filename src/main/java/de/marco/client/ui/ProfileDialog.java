package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperDialog;

import de.marco.shared.models.Utente;

public class ProfileDialog extends PaperDialog {

	private static CreateUserFormUiBinder uiBinder = GWT.create(CreateUserFormUiBinder.class);

	interface CreateUserFormUiBinder extends UiBinder<Widget, ProfileDialog> {
	}

	private Utente utente = null;

	@UiField
	DivElement username;
	@UiField
	DivElement useremail;

	@UiField
	DivElement nome;
	@UiField
	DivElement cognome;
	@UiField
	DivElement sesso;
	@UiField
	DivElement dataDiNascita;
	@UiField
	DivElement luogoDiNascita;
	@UiField
	DivElement indirizzo;

	public ProfileDialog(String st) {
		super(st);
		add(uiBinder.createAndBindUi(this));

		this.setEntryAnimation("fade-in-animation");
		this.addStyleName("profile-dialog");
		this.setModal(true);
		this.setHeight("70%");

	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	@Override
	public void open() {
		// Before showing the panel, we want to have all
		// its fields populated
		username.setInnerText(utente.getUsername());
		useremail.setInnerText(utente.getEmail());
		nome.setInnerText(utente.getNome());
		cognome.setInnerText(utente.getCognome());
		sesso.setInnerText(utente.getSesso());
		dataDiNascita.setInnerText(utente.getDataDiNascita());
		luogoDiNascita.setInnerText(utente.getLuogoDiNascita());
		indirizzo.setInnerText(utente.getIndirizzo());

		super.open();
	}

	@UiHandler("dismiss")
	void closeDialog(ClickEvent ev) {
		
		this.close();
	}
}