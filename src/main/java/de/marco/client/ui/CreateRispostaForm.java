package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperButton;
import com.vaadin.polymer.paper.widget.PaperDialog;
import com.vaadin.polymer.paper.widget.PaperTextarea;
import com.vaadin.polymer.paper.widget.PaperToast;

import de.marco.client.AppController;
import de.marco.shared.UnauthorizedException;
import de.marco.shared.models.Risposta;
import de.marco.shared.models.RispostaImpl;

public class CreateRispostaForm extends PaperDialog {

	private static CreateRispostaFormUiBinder uiBinder = GWT.create(CreateRispostaFormUiBinder.class);

	interface CreateRispostaFormUiBinder extends UiBinder<Widget, CreateRispostaForm> {
	}

	private String domandaId = null;

	@UiField PaperTextarea rispostaText;
	@UiField PaperButton save;
	@UiField PaperButton cancel;

	public CreateRispostaForm(String st) {
		super(st);
		add(uiBinder.createAndBindUi(this));
		this.setEntryAnimation("fade-in-animation");
		this.addStyleName("login-dialog");
		this.setModal(true);
		this.setHeight("70%");
	}

	public void setDomandaId(String domandaId) {
		this.domandaId = domandaId;
	}

	@UiHandler("cancel")
	void closeDialog(ClickEvent ev) {
		rispostaText.setValue("");
		this.close();
	}

	@UiHandler("save")
	void save(ClickEvent ev) {
		Risposta r = GWT.create(RispostaImpl.class);
		r.setTesto(rispostaText.getValue());

		try {
			AppController.INSTANCE.domandaService.addRisposta(domandaId, r, new AsyncCallback<Void>() {

				private HTMLPanel mainPanel = AppController.INSTANCE.getMainPanel();

				@Override
				public void onSuccess(Void result) {
					closeDialog(null);

					PaperToast toast = new PaperToast();
					toast.setText("Risposta salvata.");

					// aggiorna e toast
					mainPanel.clear();
					mainPanel.add(new DomandaDetail(domandaId));
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); // Very ugly lambda
				}

				@Override
				public void onFailure(Throwable caught) {
					closeDialog(null);

					PaperToast toast = new PaperToast();
					toast.setText("Solo un utente registrato può inserire una risposta.");
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); // lambda

				}
			});
		} catch (UnauthorizedException e) {
			// l'utente non è registrato
			closeDialog(null);

			PaperToast toast = new PaperToast();
			toast.setText("Solo un utente registrato può inserire una risposta.");
			AppController.INSTANCE.getMainPanel().add(toast);

			toast.ready((arg) -> {
				toast.open();
				return null;
			}); // lambda
		}

	}

}
