package de.marco.client.ui;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import de.marco.client.AppController;
import de.marco.shared.models.Domanda;

public class DomandaListView extends Composite {

	private static DomandaListViewUiBinder uiBinder = GWT.create(DomandaListViewUiBinder.class);

	interface DomandaListViewUiBinder extends UiBinder<Widget, DomandaListView> {
	}

	
	@UiField VerticalPanel domandaList;
	@UiField DivElement emptyMsg;
	
	public DomandaListView() { //chiede al server la lista delle domande, se ci sono
		initWidget(uiBinder.createAndBindUi(this));
		AppController.INSTANCE.domandaService.getDomande(new AsyncCallback<List<Domanda>>() {
			
			@Override
			public void onSuccess(List<Domanda> result) {
				
				if (result.isEmpty()) {
					return;
				}
				emptyMsg.removeFromParent(); // remove the empty message
				 
				for (Domanda d : result) {
					domandaList.add(new DomandaListElement(d));
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
