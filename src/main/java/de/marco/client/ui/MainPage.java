package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.vaadin.polymer.paper.widget.PaperDialog;
import com.vaadin.polymer.paper.widget.PaperDrawerPanel;
import com.vaadin.polymer.paper.widget.PaperFab;
import com.vaadin.polymer.paper.widget.PaperIconItem;
import com.vaadin.polymer.paper.widget.PaperToast;

import de.marco.client.AppController;
import de.marco.shared.models.Utente;

public class MainPage extends Composite {

	private static MainPageUiBinder uiBinder = GWT.create(MainPageUiBinder.class);

	interface MainPageUiBinder extends UiBinder<HTMLPanel, MainPage> {
	}

	// Drawer menu panel
	@UiField PaperDrawerPanel drawerPanel;

	// Page content Panel
	@UiField HTMLPanel contentPanel;

	// Menu items
	@UiField PaperIconItem menuRegister;
	@UiField PaperIconItem menuLogin;
	@UiField PaperIconItem listaUsers;
	@UiField PaperIconItem menuLogout;
	@UiField PaperIconItem menuProfile;
	@UiField PaperFab newDomandaDisable;
	@UiField PaperFab newDomanda;

	// Forms
	@UiField PaperDialog createUserForm;
	@UiField PaperDialog loginForm;
	@UiField PaperDialog domandaForm;
	@UiField PaperDialog profileDialog;
	@UiField UsersList utentiListDialog;

	public MainPage() {
		initWidget(uiBinder.createAndBindUi(this));
		AppController.INSTANCE.setMainPanel(contentPanel);
		AppController.INSTANCE.setMainPage(this);
		refreshMenu();
		contentPanel.add(new DomandaListView());
	}

	public void refreshMenu() {
		AppController.INSTANCE.loginService.isLoggedIn(new AsyncCallback<Boolean>() {

			@Override
			public void onSuccess(Boolean isLoggedIn) {
				if (isLoggedIn) {
					menuLogout.setVisible(true);
					menuProfile.setVisible(true);
					menuLogin.setVisible(false);
					menuRegister.setVisible(false);
					listaUsers.setVisible(true);
					newDomanda.setVisible(true);
					newDomandaDisable.setVisible(false);
					
				} else {
					menuLogout.setVisible(false);
					menuProfile.setVisible(false);
					menuLogin.setVisible(true);
					menuRegister.setVisible(true);
					listaUsers.setVisible(false);
					newDomanda.setVisible(false);
					newDomandaDisable.setVisible(true);
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Can't decide if user is logged in");
			}
		});
	}

	@UiHandler("menuRegister")
	public void openRegistrationMenu(ClickEvent e) {
		GWT.log("CIAONE");
		drawerPanel.closeDrawer();
		createUserForm.open();
	}

	@UiHandler("menuProfile")
	public void showProfile(ClickEvent e) {
		drawerPanel.closeDrawer();
		
		String username = AppController.INSTANCE.getLoggedInUser();
		AppController.INSTANCE.userService.getUtenteByName(username, new AsyncCallback<Utente>() {
			
			@Override
			public void onSuccess(Utente result) {
				((ProfileDialog) profileDialog).setUtente(result);
				profileDialog.open();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Woohps");
			}
		});
		
	}
	
	@UiHandler("menuLogin")
	void openLoginMenu(ClickEvent ev) {
		drawerPanel.closeDrawer();
		loginForm.open();
	}
	
	
	@UiHandler("menuLogout")
	void doLogout(ClickEvent ev) {
		drawerPanel.closeDrawer();
		AppController.INSTANCE.loginService.logout(new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				AppController.INSTANCE.setLoggedInUser(null);
				PaperToast toast = new PaperToast("Logout avvenuto con successo.");
				// Back to homepage
				contentPanel.clear();
				contentPanel.add(new DomandaListView());
				contentPanel.add(toast);
				toast.ready((arg) -> {toast.open(); return null;});
				refreshMenu();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				PaperToast toast = new PaperToast("C'è stato un problema nell'effettuare il logout.");
				contentPanel.add(toast);
				toast.ready((arg) -> {toast.open(); return null;});
			}
		});
	}
	
	@UiHandler("newDomanda")
	void addDomanda(ClickEvent ev) {
		domandaForm.open();
	}
	@UiHandler("listaUsers")
	void addlist(ClickEvent ev) {
		utentiListDialog.open();
	}

}
