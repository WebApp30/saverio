package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

import de.marco.shared.models.Utente;

public class UtentiListElement extends Composite {

	private static UtentiListElementUiBinder uiBinder = GWT.create(UtentiListElementUiBinder.class);

	interface UtentiListElementUiBinder extends UiBinder<Widget, UtentiListElement> {
	}
	@UiField DivElement username;
	public UtentiListElement(Utente u) {
		initWidget(uiBinder.createAndBindUi(this));
		username.setInnerText(u.getUsername()); 
	}
	
}
