package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperButton;
import com.vaadin.polymer.paper.widget.PaperDialog;
import com.vaadin.polymer.paper.widget.PaperTextarea;
import com.vaadin.polymer.paper.widget.PaperToast;

import de.marco.client.AppController;
import de.marco.shared.UnauthorizedException;
import de.marco.shared.models.Domanda;
import de.marco.shared.models.DomandaImpl;

public class CreateDomandaForm extends PaperDialog {

	private static CreateDomandaFormUiBinder uiBinder = GWT.create(CreateDomandaFormUiBinder.class);

	interface CreateDomandaFormUiBinder extends UiBinder<Widget, CreateDomandaForm> {
	}

	@UiField PaperTextarea domandaText;
	@UiField PaperButton save;
	@UiField PaperButton cancel;

	public CreateDomandaForm(String st) {
		super(st);
		add(uiBinder.createAndBindUi(this));
		this.setEntryAnimation("fade-in-animation");
		this.addStyleName("login-dialog");
		this.setModal(true);
		this.setHeight("70%");
	}

	@UiHandler("cancel")
	void closeDialog(ClickEvent ev) {
		domandaText.setValue("");
		this.close();
	}

	
	@UiHandler("save")
	void save(ClickEvent ev) {
		String username = AppController.INSTANCE.getLoggedInUser();
		Domanda d = GWT.create(DomandaImpl.class);
		d.setAutore(username);
		d.setTesto(domandaText.getValue());
		
		
		try {
			AppController.INSTANCE.domandaService.addDomanda(d, new AsyncCallback<Void>() {

				private HTMLPanel mainPanel =  AppController.INSTANCE.getMainPanel();
				
				@Override
				public void onSuccess(Void result) {
					closeDialog(null);

					 
					PaperToast toast = new PaperToast();
					toast.setText("Domanda aggiunta.");
					
					// aggiorna lista e toast
					mainPanel.clear();
					mainPanel.add(new DomandaListView());
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); // lambda

				}

				@Override
				public void onFailure(Throwable caught) {
					closeDialog(null);

					PaperToast toast = new PaperToast();
					toast.setText("Inserimento della domanda fallito.");
					mainPanel.add(toast);

					toast.ready((arg) -> {
						toast.open();
						return null;
					}); // lambda

				}
			});
		} catch (UnauthorizedException e) {
			// l'utente non è registrato
			closeDialog(null);

			PaperToast toast = new PaperToast();
			toast.setText("Solo un utente registrato può inserire una domanda.");
			AppController.INSTANCE.getMainPanel().add(toast);

			toast.ready((arg) -> {
				toast.open();
				return null;
			}); // lambda
		}
	}
	
	@UiHandler("dismiss") 
	void closeDialog2(ClickEvent ev) {
		
		this.close();
	}
}
