package de.marco.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperButton;
import com.vaadin.polymer.paper.widget.PaperDialog;
import com.vaadin.polymer.paper.widget.PaperDropdownMenu;
import com.vaadin.polymer.paper.widget.PaperInput;
import com.vaadin.polymer.paper.widget.PaperItem;
import com.vaadin.polymer.paper.widget.PaperMenu;
import com.vaadin.polymer.paper.widget.PaperToast;
import com.vaadin.polymer.vaadin.widget.VaadinDatePicker;

import de.marco.client.AppController;
import de.marco.shared.FieldVerifier;
import de.marco.shared.models.Utente;
import de.marco.shared.models.UtenteImpl;

public class CreateUserForm extends PaperDialog {

	private static CreateUserFormUiBinder uiBinder = GWT.create(CreateUserFormUiBinder.class);

	interface CreateUserFormUiBinder extends UiBinder<Widget, CreateUserForm> {
	}

	private static String[] genders = { "", "M", "F" };

	@UiField
	PaperInput username;
	@UiField
	PaperInput userpass;
	@UiField
	PaperInput useremail;
	
	@UiField
	PaperInput nome;
	@UiField
	PaperInput cognome;

	/*
	 * sesso is the dropdown container, used to access the result while
	 * sessoMenu is the menu widget used to visualize the options
	 */
	@UiField
	PaperDropdownMenu sesso;
	@UiField
	PaperMenu sessoMenu;

	@UiField
	VaadinDatePicker dataDiNascita;
	@UiField
	PaperInput luogoDiNascita;
	@UiField
	PaperInput indirizzo;

	@UiField
	PaperButton confirmAddButton;

	public CreateUserForm(String st) {
		super(st);
		add(uiBinder.createAndBindUi(this));

		this.setEntryAnimation("fade-in-animation");
		this.addStyleName("registration-dialog");
		this.setModal(true);
		this.setHeight("90%");

		for (String s : genders) {
			sessoMenu.add(new PaperItem(s));
		}

		GWT.log(st);

	}

	@UiHandler("confirmAddButton")
	protected void confirmAdd(ClickEvent ev) {
		Utente u = new UtenteImpl(username.getValue(), userpass.getValue(), useremail.getValue());
		
		u.setNome(nome.getValue());
		u.setCognome(cognome.getValue());
		u.setSesso(sesso.getSelectedItemLabel());
		u.setDataDiNascita(dataDiNascita.getValue());
		u.setLuogoDiNascita(luogoDiNascita.getValue());
		u.setIndirizzo(indirizzo.getValue());

		// se c'è un problema, do nothing and return
		if (!(useremail.validate() && FieldVerifier.isValidUtente(u))) {
			GWT.log("Invalid user");
			return;
		}

		// this.clear(); problema blocco
		this.close();

		AppController.INSTANCE.userService.addUtente(u, new AsyncCallback<Boolean>() {

			@Override
			public void onSuccess(Boolean result) {
				GWT.log("OK!");
				if (result) {
					PaperToast toast = new PaperToast(" Nuovo Utente creato con successo  ");
					AppController.INSTANCE.getMainPanel().add(toast);
					toast.ready((arg) -> {
						toast.open();
						return null;
					});

				} else {
					PaperToast toast = new PaperToast(" Creazione nuovo utente fallito : User già esistente, riprova con un altro  ");
					AppController.INSTANCE.getMainPanel().add(toast);
					toast.ready((arg) -> {
						toast.open();
						return null;
					});
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("fail");
			}
		});
	}

	@UiHandler("dismiss")
	void closeDialog(ClickEvent ev) {

		this.close();
	}

}