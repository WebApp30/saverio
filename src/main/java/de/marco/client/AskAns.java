package de.marco.client;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.UmbrellaException;
import com.google.gwt.user.client.ui.RootPanel;
import com.vaadin.polymer.Polymer;
import com.vaadin.polymer.paper.PaperDialogElement;

import de.marco.client.ui.MainPage;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class AskAns implements EntryPoint {

	@Override
	public void onModuleLoad() {

		GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
			@Override
			public void onUncaughtException(@NotNull Throwable e) {
				ensureNotUmbrellaError(e);
			}
		});

		Polymer.importHref(Arrays.asList(
			    "iron-icons/iron-icons.html",
			    "vaadin-icons/vaadin-icons.html", // import any icon collection in this way
			    "iron-icon",
			    PaperDialogElement.SRC
			));
		
		RootPanel.get().add(new MainPage());
	}
/* che fatica per chrome
 * http://stackoverflow.com/questions/11029002/gwt-client-umbrellaexception-get-full-error-message-in-java
 */
	private static void ensureNotUmbrellaError(@NotNull Throwable e) {
		for (Throwable th : ((UmbrellaException) e).getCauses()) {
			if (th instanceof UmbrellaException) {
				ensureNotUmbrellaError(th);
			} else {
				th.printStackTrace();
			}
		}
	}
}
