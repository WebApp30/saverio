package de.marco.client.ws;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.marco.shared.UnauthorizedException;
import de.marco.shared.models.Domanda;
import de.marco.shared.models.Risposta;

@RemoteServiceRelativePath("questions")
public interface DomandaService extends RemoteService {

	List<Domanda> getDomande();

	void addDomanda(Domanda d) throws UnauthorizedException;
	
	void removeDomanda(String domandaId) throws UnauthorizedException;
	
	Domanda getDomandaById(String id);
	
	void addRisposta(String domandaId, Risposta r) throws UnauthorizedException;
	
	void removeRisposta(String domandaId, Risposta r) throws UnauthorizedException;
	
}
