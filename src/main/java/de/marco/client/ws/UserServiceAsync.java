package de.marco.client.ws;

import java.util.List;   

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.marco.shared.models.Utente; 

public interface UserServiceAsync {
	void addUtente(Utente u, AsyncCallback<Boolean> callback);

	void getUtenti(AsyncCallback<List<Utente>> callback);
	
	void getUtenteByName(String username, AsyncCallback<Utente> callback);

	void utenteExists(Utente u, AsyncCallback<Boolean> callback);
}
