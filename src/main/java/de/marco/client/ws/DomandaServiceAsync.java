package de.marco.client.ws;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.marco.shared.UnauthorizedException;
import de.marco.shared.models.Domanda;
import de.marco.shared.models.Risposta;

public interface DomandaServiceAsync {

	void getDomande(AsyncCallback<List<Domanda>> callback);

	void addDomanda(Domanda d, AsyncCallback<Void> callback) throws UnauthorizedException;
	
	void removeDomanda(String domandaId, AsyncCallback<Void> callback) throws UnauthorizedException;
	
	void getDomandaById(String id, AsyncCallback<Domanda> callback);
	
	void addRisposta(String id, Risposta r, AsyncCallback<Void> callback) throws UnauthorizedException;
	
	void removeRisposta(String domandaId, Risposta r, AsyncCallback<Void> callback) throws UnauthorizedException;
}
