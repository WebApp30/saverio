package de.marco.client.ws;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("login")
public interface LoginService extends RemoteService {
	
	public boolean login(String username, String password);
	
	public void logout();
	
	public String getLoggedInUserName();

	public boolean isLoggedIn();
	
}
