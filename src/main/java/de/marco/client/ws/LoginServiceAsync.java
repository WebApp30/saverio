package de.marco.client.ws;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface LoginServiceAsync {
	
	public void login(String username, String password, AsyncCallback<Boolean> callback);
	
	public void logout(AsyncCallback<Void> callback);
	
	public void getLoggedInUserName(AsyncCallback<String> callback);

	public void isLoggedIn(AsyncCallback<Boolean> callback);
}
