package de.marco.client.ws;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import de.marco.shared.models.Utente;

@RemoteServiceRelativePath("users")
public interface UserService extends RemoteService {
	public boolean addUtente(Utente u);

	public List<Utente> getUtenti();
	
	public Utente getUtenteByName(String username);

	public boolean utenteExists(Utente u);
}
