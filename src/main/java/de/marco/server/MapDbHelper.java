package de.marco.server;

import java.io.File;
import java.util.Map;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import de.marco.shared.models.Domanda;
import de.marco.shared.models.Utente;


/**
 * Implements a singleton using an enum, che ci garantisce la singronizzazione
 * trovato qui
 * {@link https://stackoverflow.com/questions/26285520/implementing-singleton-with-an-enum-in-java}
 */
public enum MapDbHelper {

	// TODO close db
	INSTANCE;

	private DB db = null;

	public DB getDB() {
		if (db == null) {
			db = DBMaker.fileDB(new File("db")).closeOnJvmShutdown().make();
		}
		return db;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Utente> retrieveUtentiMap() {
		return getDB().hashMap("utenti", Serializer.STRING, Serializer.JAVA).createOrOpen();
	}

	@SuppressWarnings("unchecked")
	public Map<String, Domanda> retrieveDomandeMap() {
		Map<String, Domanda> domandeMap = getDB().treeMap("domande", Serializer.STRING, Serializer.JAVA).createOrOpen();
		return domandeMap;
	}
	
}
