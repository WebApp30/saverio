package de.marco.server;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.mapdb.DB;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.marco.client.ws.DomandaService;
import de.marco.shared.UnauthorizedException;
import de.marco.shared.models.Domanda;
import de.marco.shared.models.Risposta;
import de.marco.shared.models.Utente;

public class DomandaServiceImpl extends RemoteServiceServlet implements DomandaService {

	private static final long serialVersionUID = 2039408538523163464L;

	@Override
	public List<Domanda> getDomande() {
		List<Domanda> domandeList = new ArrayList<>();
		Map<String, Domanda> domandeMap = MapDbHelper.INSTANCE.retrieveDomandeMap();

		Set<String> keys = domandeMap.keySet();
		for (String k : keys) {
			domandeList.add(domandeMap.get(k));
		}
		domandeList.sort(new DomandaComparatorInverse());

		return domandeList;
	}

	@Override
	public void addDomanda(Domanda d) throws UnauthorizedException {  //controlla che sia loggato

		if (!isLoggedIn()) {
			throw new UnauthorizedException();  
		}

		DB db = MapDbHelper.INSTANCE.getDB();  //ottiene db
		Map<String, Domanda> domandeMap = MapDbHelper.INSTANCE.retrieveDomandeMap(); //ottiene la mappa di tutte le domande

	    // genera un id da 128 bit.
		d.setId(UUID.randomUUID().toString());

		// passa l'utente loggato
		d.setAutore(getLoggedUser().getUsername());

		// Use java.util.Date because it's supported client-side
		Date now = new Date();
		d.setTime(now);
		// crea spazio per eventuali risposte
		d.setRisposte(new ArrayList<>());

		domandeMap.put(d.getId(), d); 
		db.commit();
	}

	@Override
	public void removeDomanda(String domandaId) throws UnauthorizedException {
		if (!isAdmin()) {
			throw new UnauthorizedException();
		}
		DB db = MapDbHelper.INSTANCE.getDB();
		Map<String, Domanda> domandeMap = MapDbHelper.INSTANCE.retrieveDomandeMap();

		domandeMap.remove(domandaId);

		db.commit();
	}

	@Override
	public Domanda getDomandaById(String id) {
		Map<String, Domanda> domandeMap = MapDbHelper.INSTANCE.retrieveDomandeMap();
		return domandeMap.get(id);
	}

	@Override
	public void addRisposta(String domandaId, Risposta r) throws UnauthorizedException {

		if (!isLoggedIn()) {
			throw new UnauthorizedException();
		}

		DB db = MapDbHelper.INSTANCE.getDB();
		Map<String, Domanda> domandeMap = MapDbHelper.INSTANCE.retrieveDomandeMap();

		Domanda d = domandeMap.get(domandaId);

		r.setId(UUID.randomUUID().toString());
		r.setTime(new Date());
		r.setAutore(getLoggedUser());
		r.setDomanda(d);

		d.getRisposte().add(r);
		domandeMap.put(d.getId(), d);

		db.commit();
	}

	@Override
	public void removeRisposta(String domandaId, Risposta risp) throws UnauthorizedException {

		if (!isAdmin()) {
			throw new UnauthorizedException();
		}
		DB db = MapDbHelper.INSTANCE.getDB();
		Map<String, Domanda> domandeMap = MapDbHelper.INSTANCE.retrieveDomandeMap();

		Domanda d = domandeMap.get(domandaId);

		List<Risposta> risposte = d.getRisposte();

		// compare ID per delete
		int size = risposte.size();
		for (int i = 0; i < size; i++) {
			Risposta risp2 = risposte.get(i);
			if (risp2.getId().equals(risp.getId())) {
				risposte.remove(i);
				break;
			}
		}

		domandeMap.put(d.getId(), d);
		db.commit();
	}

	private boolean isLoggedIn() {
		HttpSession session = this.getThreadLocalRequest().getSession(false);

		if (session == null || session.getAttribute("user") == null)
			return false;

		return true;
	}

	private boolean isAdmin() {
		// If we use this, we should already have checked for a valid session
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		return session.getAttribute("type").equals("admin");
	}

	private Utente getLoggedUser() {
		// If we use this, we should already have checked for a valid session
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		return (Utente) session.getAttribute("user");
	}

	/**
	 * implementa compare inverse di Domanda
	 */
	private class DomandaComparatorInverse implements Comparator<Domanda> {

		@Override
		public int compare(Domanda d1, Domanda d2) {
			return d2.getTime().compareTo(d1.getTime());
		}

	}

}
