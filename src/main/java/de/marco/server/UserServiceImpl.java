package de.marco.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mapdb.DB;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.marco.client.ws.UserService;
import de.marco.shared.models.Utente;


public class UserServiceImpl extends RemoteServiceServlet implements UserService {

	private static final long serialVersionUID = 1015914920292810061L;

	@Override
	public boolean addUtente(Utente u) {
		
		if ( u.getUsername().equals("admin") )
				return false;

			
			DB db = MapDbHelper.INSTANCE.getDB();
		Map<String, Utente> utenti = MapDbHelper.INSTANCE.retrieveUtentiMap();
		if ( utenti.get(u.getUsername()) == null)
		{
			utenti.put(u.getUsername(), u);
			db.commit();
			return true;
		}
		 return false;
	}

	@Override
	public List<Utente> getUtenti() {
		List<Utente> utentiList = new ArrayList<>();
		Map<String, Utente> utentiMap = MapDbHelper.INSTANCE.retrieveUtentiMap();

		Set<String> keys = utentiMap.keySet();
		for (String k : keys) {
			utentiList.add(utentiMap.get(k));
		}

		return utentiList;
	}

	@Override
	public Utente getUtenteByName(String username) {
		Map<String, Utente> utentiMap = MapDbHelper.INSTANCE.retrieveUtentiMap();
		return utentiMap.get(username);
	}
	
	@Override
	public boolean utenteExists(Utente u) {
		Map<String, Utente> utentiMap = MapDbHelper.INSTANCE.retrieveUtentiMap();
		return (utentiMap.get(u.getUsername()) != null);
	}

}
