package de.marco.server;

import javax.servlet.http.HttpSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import de.marco.client.ws.LoginService;
import de.marco.client.ws.UserService;
import de.marco.shared.models.Utente;
import de.marco.shared.models.UtenteImpl;


public class LoginServiceImpl extends RemoteServiceServlet implements LoginService {

	private static final long serialVersionUID = -4511922018375621417L;

	@Override
	public boolean login(String username, String password) {

		// admin 
		if (username.equals("admin") && password.equals("admin")) {
			HttpSession session = this.getThreadLocalRequest().getSession();
			Utente u = new UtenteImpl();
			u.setUsername("admin");
			session.setAttribute("user", u);
			session.setAttribute("type", "admin");
			return true;
		}
		
		UserService userService = new UserServiceImpl();
		Utente u = userService.getUtenteByName(username);
		
		
		
		
		
		if(u != null && u.getPassword().equals(password)) {
			HttpSession session = this.getThreadLocalRequest().getSession();
			session.setAttribute("user", u);
			session.setAttribute("type", "any");
			return true;
		}
		
		return false;
	}

	@Override
	public void logout() {
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		if (session != null) {
			session.invalidate();
		}
	}

	@Override
	public String getLoggedInUserName() {
		String name = null;
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		if (session != null) {
			Utente u = (Utente) session.getAttribute("user");
			if (u != null) name = u.getUsername();
		}
		return name;
	}
	
	@Override
	public boolean isLoggedIn() {
		HttpSession session = this.getThreadLocalRequest().getSession(false);
		return session != null;
	}

}
